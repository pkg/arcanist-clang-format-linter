<?php

arcanist_load_libraries(
    ["/usr/share/arcanist/externals/includes/clang-format-linter/"],
    $must_load = false,
    $lib_source = "clang-format-linter",
    ArcanistWorkingCopyIdentity::newDummyWorkingCopy());

?>
